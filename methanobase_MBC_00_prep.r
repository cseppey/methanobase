#%%%%
# Methanobase MBC preparation
#%%%%

rm(list=ls(all.names=T))

setwd('~')

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(xtable)
require(rgeos)
require(maptools)
require(geosphere) # e.g. distm
require(raster) # e.g. stack, crop, extent
require(RColorBrewer) # brewer.pal 
require(igraph)
require(Hmisc)
require(PMCMR)
require(multcompView)
require(zCompositions)
require(abind)

# prep cluster

nb_cpu <- 5

if(exists('cl')){
  stopCluster(cl)
}

cl <- makeSOCKcluster(nb_cpu)

clusterEvalQ(cl, library(raster))

registerDoSNOW(cl)


# DIR LOAD ####
dir_proj <- 'Projets/Methanobase/'
dir_stat <- paste0(dir_proj, 'stat/')
dir_in <- paste0(dir_proj, 'data/')
dir_out <- paste0(dir_stat, 'out/00_prep/')
dir_save <- paste0(dir_stat, 'Rdata/')

dir.create(dir_out, showWarnings=F, recursive=T)
dir.create(dir_save, showWarnings=F, recursive=T)

source(paste0(dir_stat, 'function_methanobase.r'))

#---
# need to supress the "'" and '#' before to read file
# names also have some discrepancies, only identical after names corrected (l92-101)
mr_ass <- read.table(paste0(dir_in, 'Galaxy10-[FROGS_BIOM_to_TSV__abundance.tsv].tsv'), h=T, sep='\t') # silva 138 swm v1

mr_ass <- mr_ass[,grep('^i', names(mr_ass), invert=T)]
mr_ass$observation_name <- paste0('C_', sapply(strsplit(as.character(mr_ass$observation_name), '_'), '[[', 2))

env_ini <- read.table(paste0(dir_in, 'var_env_methanobase.csv'), h=T, sep='\t', row.names=1)

fa_tot <- read.table(paste0(dir_in, '/out.falin'), sep='\t', row.names=1) # retreived from Galaxy12-[FROGS_Filters__sequences.fasta].fasta

dimnames(mr_ass) <- list(mr_ass$observation_name, gsub('SIL2DST[0-9]', 'SIL2DST', names(mr_ass), perl=T)) # bug with SIL2DST

# mr names ####
mr_tot <- as.data.frame(t(mr_ass[,grep('^[A-Z]', names(mr_ass))]))
names(mr_tot) <- mr_ass$observation_name

# differenciating samples from a same sample
rn <- sapply(strsplit(row.names(mr_tot), '_'), '[[', 1)

inde <- 0
for(i in unique(rn)){
  inde <- inde+1
  
  ind_rn <- which(rn == i)
  if(length(ind_rn) > 1){
    for(j in 1:length(ind_rn)){
      rn[ind_rn[j]] <- paste0(rn[ind_rn[j]], j)
    }
    ind <- which(row.names(env_ini) == i)
    env_ini <- rbind.data.frame(env_ini[1:ind,], env_ini[ind:nrow(env_ini),])
    row.names(env_ini)[c(ind, ind+1)] <- paste0(row.names(env_ini)[ind], 1:length(ind_rn))
  }
}

rn[rn == 'ALS9DST'] <- 'ALS9CST' ### ### ### ### ### ###
rn[rn == 'ALP2DW']  <- 'ALP2DWT'
rn[rn == 'ALP5DW']  <- 'ALP5DWT'
rn[rn == 'PCL2BW']  <- 'PCL2BWB'
rn[rn == 'PCP2AW']  <- 'PCP2AWT'
rn[rn == 'PCS1BS']  <- 'PCS1BSB'
rn[rn == 'PPL2AWB'] <- 'PPL2AWT'
rn[rn == 'PPL2BWB'] <- 'PPL2BWT'
rn[rn == 'PTP2AW']  <- 'PTP2AWT'
rn[rn == 'PTP2DW']  <- 'PTP2DWT'

row.names(mr_tot) <- rn
#

# env ####
env_tot <- env_ini[row.names(mr_tot),]

# buiding env_tot2
env_tot2 <- as.data.frame(matrix(nrow=nrow(mr_tot),ncol=0), row.names=row.names(mr_tot))

env_tot2$geo_loc_name <- as.factor(sapply(strsplit(as.character(env_tot$geo_loc_name), ' '), '[[', 2))

env_tot2$env_feature <- factor(sapply(strsplit(as.character(env_tot$env_feature), ' '), '[[', 1))
env_tot2$env_feature <- factor(env_tot2$env_feature, levels=levels(env_tot2$env_feature)[c(1,5,2,3,7,6,8,4)])

env_tot2$env_mat_OM <- factor(sapply(strsplit(as.character(env_tot$env_material), ' '), '[[', 1))
env_tot2$env_mat_OM[(env_tot2$env_mat_OM == 'organic' | env_tot2$env_mat_OM == 'mineral') & env_tot$LOI >= 0.4] <- 'organic'
env_tot2$env_mat_OM[(env_tot2$env_mat_OM == 'organic' | env_tot2$env_mat_OM == 'mineral') & env_tot$LOI < 0.4] <- 'mineral'

env_tot2$microtopography <- factor(env_tot$microtopography, levels=levels(env_tot$microtopography)[c(5,3,2,1,4)])

env_tot$depth_class[grepl('[[:digit:]]', env_tot$depth_class)] <- NA

env_tot2$latitude <- env_tot$latitude
env_tot2$longitude <- env_tot$longitude

env_tot2$depth_soil <- env_tot$depth_soil

env_tot2$depth_water <- env_tot$depth_water

env_tot2$dry_wgh <- env_tot$dry_wgh

env_tot2$temp_deg_C <- env_tot$temp_deg_C

env_tot2$prokabu <- env_tot$Bacteria_abundance + env_tot$Archaea_abundance
env_tot2$pmoA <- env_tot$Methanotrophs_abundance
env_tot2$mcrA <- env_tot$Methanogens_abundance

env_tot2$pH <- env_tot$pH

env_tot2$OM <- env_tot$LOI
env_tot2$OM[env_tot2$OM > 1] <- 1

env_tot2$diss_oxygen <- env_tot$diss_oxygen

env_tot2$diss_CH4_mg_L <- env_tot$diss_CH4_mg_L

env_tot2$redox_potential <- env_tot$redox_potential

env_tot2$conduc_mS_cm <- env_tot$conduc_mS_cm

env_tot2$nb_seq <- rowSums(mr_tot)


env_tot2 <- droplevels(env_tot2)

# remove smps with too high conductivity ----
ind_high_c <- which(env_tot2$conduc_mS_cm > 2000)
env_tot2 <-env_tot2[-c(ind_high_c),] 

mr_tot <- mr_tot[-c(ind_high_c),]
mr_tot <- mr_tot[,colSums(mr_tot) != 0]

# dist_geo ----
d_geo <- distm(env_tot2[,c('longitude','latitude')])

dimnames(d_geo) <- list(row.names(env_tot2), row.names(env_tot2))

d_geo <- as.dist(d_geo)

PCNM <- pcnm(d_geo, threshold=max(d_geo))$vector

env_tot2 <- data.frame(env_tot2, PCNM)


# bioclim ----
stack <- stack()
for(j in 1:19){
  jc <- ifelse(j < 10, paste0(0,j), as.character(j))
  print(jc)
  stack <- stack(list(stack, raster(paste0('MAP/bioclim_V2_30_seq_2017/wc2.0_bio_30s_', jc, '.tif'))))
}

bc <- foreach(i=levels(env_tot2$geo_loc_name), .verbose=T, .combine=rbind) %dopar% {
  # get the ind of the region
  ind_reg <- which(env_tot2$geo_loc_name == i)
  rng <- sapply(env_tot2[c('longitude','latitude')][ind_reg,], function(x) range(x))
  rng <- rng+c(-0.1,0.1)
  
  # crop the stack accordingly
  sub_stack <- crop(stack, extent(t(rng)))

  df <- as.data.frame(rasterToPoints(sub_stack))
  
  # retreive the bioclim var for the closest pixel of the sampling coordinate
  bcl <- NULL
  for(j in ind_reg){
    dif_x <- abs(df$x-env_tot2$longitude[j])
    dif_y <- abs(df$y-env_tot2$latitude[j])
    
    x <- which(dif_x == min(dif_x))
    y <- which(dif_y == min(dif_y))
    
    # debug points falling in ocean
    ind_coord <- intersect(x,y)
    if(length(ind_coord) == 0){
      d <- abs(x-max(y))
      ind_coord <- x[which(d == min(d))]
    }
    
    bcl <- rbind(bcl, df[ind_coord,])
  }
  
  return(bcl)
  
}

row.names(bc) <- row.names(env_tot2)
names(bc)[-c(1:2)] <- apply(sapply(strsplit(names(bc)[-c(1:2)], '_'), '[', c(2,4)), 2, function(x) paste(x, collapse='_'))

env_tot2 <- data.frame(env_tot2[,], bc[,grepl('bio',names(bc))])


# grp variables ----
regex_var <- c(geo='PCNM',
               phy_chi=paste(c('dry_wgh','temp_deg_C','pH','OM','env_mat_OM','depth',
                              'diss_oxygen','diss_CH4_mg_L','redox_potential','conduc_mS_cm'), collapse='|'),
               meteo='bio_')


# palette ----
lev_reg <- levels(env_tot2$geo_loc_name)
col_reg <- brewer.pal(length(lev_reg), 'Dark2')
names(col_reg) <- lev_reg

lev_hab <- levels(env_tot2$env_feature)
col_hab <- brewer.pal(length(lev_hab), 'Set1')[c(1,5,6,3,7,4,8,2)]
names(col_hab) <- lev_hab

lev_micro <- levels(env_tot2$microtopography)
col_micro <- brewer.pal(length(lev_micro), 'Set2')[c(2,4,1,5,3)]
names(col_micro) <- lev_micro

lev_mat <- levels(env_tot2$env_mat_OM)
col_mat <- brewer.pal(length(lev_mat)+4, 'Accent')[c(3,4,2,1)]
names(col_mat) <- lev_mat

lev_var <- names(regex_var)
col_var <- c('magenta','cyan','yellow')
names(col_var) <- lev_var

lst_pal <- list(geo_loc_name=col_reg, env_mat_OM=col_mat, env_feature=col_hab, microtopography=col_micro, variables=col_var)

# levels
lev_fact <- lapply(lst_pal, names)
names(lev_fact) <- c('reg','mat','feat','micro')

# grp_smp
grp_mat <- c('[a-z]', lev_fact$mat)

grp_reg <- c(paste(lev_fact$reg, collapse='|'), lev_fact$reg)


# pca on bioclim variables ----
pca <- rda(env_tot2[,grep('bio', names(env_tot2))], scale=T)
s <- summary(pca)
site <- s$sites[,1:2]
spec <- s$species[,1:2]

#---
cairo_ps(paste0(dir_out, 'pca_bio.eps'))
par(mar=c(5,4,1,1))

plot(NA, xlim=range(spec[,1], site[,1])*1.1, ylim=range(spec[,2], site[,2]), xlab='PCA1', ylab='PCA2')

abline(h=0,v=0,lty=3)

points(site, pch=19, col=lst_pal$geo_loc_name[as.numeric(env_tot2$geo_loc_name)])
text(spec, labels=row.names(spec), col=ifelse(grepl('_01$|_05$|_08$|_12$|_13$', row.names(spec)), 2, 1))

dev.off()

# network on bioclim ----
cor_bc <- rcorr(as.matrix(env_tot2[,grep('bio', names(env_tot2))]), type='spear')

rho <- abs(cor_bc$r)
rho <- ifelse(rho < 0.7, 0, rho)

gr <- graph.adjacency(rho, weight=T, mode="undirected")
gr <- simplify(gr)

V(gr)$label <- V(gr)$name

write.graph(gr, paste(dir_out, 'net_bc.graphml', sep=''), format='graphml')


# PCNM vs bioclim ----
res_coord <- 3

crd_low_res <- apply(env_tot2[,c('longitude','latitude')], 1, function(x) paste(round(x, digits=res_coord), collapse='#'))
tb_low_res <- table(crd_low_res)
crd_uniq <- sapply(strsplit(names(tb_low_res), '#'), as.numeric)
crd_uniq <- data.frame(longitude=crd_uniq[1,], latitude=crd_uniq[2,], coord_sz=as.numeric(tb_low_res), 
                       geo_loc_name=tapply(as.character(env_tot2$geo_loc_name), list(crd_low_res), unique))

# bioclim
bcl_selec <- c(1,5,8,12,13)

#---
lst_bcl <- foreach(i=bcl_selec, .verbose=T) %dopar% {
  
  print(i)
  
  thresh <- switch(as.character(i),
                   '1'  = c(0, 0, 0),
                   '5'  = c(25, 100, NA),
                   '8'  = c(0, 0, 0),
                   '12' = c(0, 0, 0),
                   '13' = c(0, 0, 0))

  # get the right stack
  sta <- stack[[i]]
  
  lst <- NULL

  for(j in lev_reg){

    print(j)
    
    # get the ind of the region
    ind_reg <- which(env_tot2$geo_loc_name == j)
    rng <- sapply(env_tot2[c('longitude','latitude')][ind_reg,], function(x) range(x))
    rng <- rng+sapply(apply(rng, 2, diff), function(x) x*c(-0.1,0.1))

    env <- env_tot2[ind_reg,]

    # crop the stack accordingly
    lst[[j]]$sub_sta <- crop(sta, extent(t(rng)))

    # remove mistake points
    lst[[j]]$sub_sta <- reclassify(lst[[j]]$sub_sta, matrix(thresh,nrow=1, byrow=T))
    
  }
  
  return(lst)
}

names(lst_bcl) <- bcl_selec

# PCNM
n_pcnm <- paste0('PCNM', 1:5)

pal_PCNM <- colorRampPalette(c('blue', 'green','red'))(1000)

pcnm01 <- unlist(env_tot2[,n_pcnm])
pcnm01 <- pcnm01-min(pcnm01)
pcnm01 <- matrix(round(pcnm01/max(pcnm01)*999+1), ncol=5)

crd_uniq <- cbind.data.frame(crd_uniq, apply(pcnm01, 2, function(x) tapply(x, list(crd_low_res), function(y) round(mean(y)))))
names(crd_uniq)[5:9] <- n_pcnm

# maps

for(h in c('PCNM', 'bcl')){
  
  cairo_ps(paste0(dir_out, h, '.eps'), 17,9)
  
  par(mfrow=c(3,5), mar=c(5,3,1,4), oma=c(0,2,4,4), xpd=NA)
  
  for(i in lev_reg){
    ind_reg <- env_tot2$geo_loc_name == i
    sub_sta <- lst_bcl[[1]][[i]]$sub_sta
    
    crd_lr <- crd_uniq[crd_uniq$geo_loc_name == i,]
    crd_tot <- env_tot2[ind_reg,c('longitude','latitude')]
    
    if(h == 'PCNM'){
      for(j in paste0('PCNM',1:5)){
        plot(reclassify(sub_sta, matrix(c(-10000,10000,NA), nrow=1)), legend.width=5)
        usr <- par('usr')
        
        if(j == 'PCNM1'){
          mtext(i, 2, 3)
        }
        if(i == 'Alaska'){
          mtext(j, 3, 2)
        }
        
        ordisurf(crd_tot[,1:2], env_tot2[ind_reg,j], add=T, col='grey20')

        points(crd_lr[,1:2], cex=crd_lr$coord_sz*0.2, pch=21, bg=pal_PCNM[crd_lr[[j]]], lwd=0.1)
        
        if(i == 'Patagonia' & j == 'PCNM5'){
          dif <- diff(usr[3:4])
          mid <- mean(usr[3:4])
          
          ly <- 7
          
          xs <- usr[2]+diff(usr[1:2])*c(0.15,0.25)
          ys <- seq(mid-dif*0.3, mid+dif*0.3, length=ly)
          
          legtxt <- round(seq(min(env_tot2[,n_pcnm]), max(env_tot2[,n_pcnm]), length=ly), digit=2)
          legcol <- colorRampPalette(c('blue', 'green','red'))(ly)
          leg_sz <- round(seq(min(crd_uniq$coord_sz), max(crd_uniq$coord_sz), length=ly))
          legcex <- seq(min(leg_sz*0.2), max(leg_sz*0.2), length=ly)
          
          par(xpd=NA)
          text(xs[1], ys, legtxt, cex=0.5, pos=2)
          points(rep(xs[1],ly), ys, pch=19, col=legcol)
          text(xs[2], ys, leg_sz, cex=0.5, pos=4, offset=0.9)
          points(rep(xs[2],ly), ys, cex=legcex)
          
          text(xs[1], max(ys)+dif*0.07, 'PCNM value', srt=90, cex=0.75, pos=4, offset=0)
          text(xs[2], max(ys)+dif*0.07, 'samples number', srt=90, cex=0.75, pos=4, offset=0)
          
        }
      }
    } else {
      for(j in as.character(bcl_selec)){
        
        plot(lst_bcl[[j]][[i]]$sub_sta, legend.width=5)
        usr <- par('usr')
        
        if(j == '1'){
          mtext(i, 2, 3)
        }
        if(i == 'Alaska'){
          mtext(paste0('bio_', j), 3, 2)
        }
        
        points(crd_lr[,1:2], cex=crd_lr$coord_sz*0.2, pch=21, bg=lst_pal$geo_loc_name[[i]], lwd=0.1)
        
        if(i == 'Patagonia' & j == '13'){
          dif <- diff(usr[3:4])
          mid <- mean(usr[3:4])
          
          ly <- 7
          
          xs <- usr[2]+diff(usr[1:2])*c(0.15,0.30)
          ys <- seq(mid-dif*0.3, mid+dif*0.3, length=ly)
          
          leg_sz <- round(seq(min(crd_uniq$coord_sz), max(crd_uniq$coord_sz), length=ly))
          legcex <- seq(min(leg_sz*0.2), max(leg_sz*0.2), length=ly)
          
          par(xpd=NA)
          text(xs[2], ys, leg_sz, cex=0.5, pos=4, offset=0.9)
          points(rep(xs[2],ly), ys, cex=legcex)
          
          text(xs[2], max(ys)+dif*0.07, 'samples number', srt=90, cex=0.75, pos=4, offset=0)
          
        }
        
      }
    }
      
  }
  
  dev.off()
}

# maps ----
data(wrld_simpl)
wbuf = gBuffer(wrld_simpl,width=0.00001)


# ass ####

ass_tot <- cbind.data.frame(taxonomy=mr_ass[,3], seq=fa_tot[,1])
row.names(ass_tot) <- row.names(mr_ass)

ass_tot <- ass_tot[names(mr_tot),]

# taxo
tax_lev <- c("reign", "phylum", "class", "order", "family", "genus", "species")
unk <- paste('unknown', tax_lev, collapse=';')
levels(ass_tot$taxonomy) <- c(levels(ass_tot$taxonomy), unk)
ass_tot$taxonomy[ass_tot$taxonomy == 'no data' | ass_tot$taxonomy == ''] <- unk

taxo_tot <- as.data.frame(matrix(unlist(strsplit(as.character(ass_tot$taxonomy), ';')), byrow=T, ncol=7))
names(taxo_tot) <- tax_lev
row.names(taxo_tot) <- row.names(ass_tot)

# sort out the unknown affiliations
taxo_tot <- sapply(taxo_tot, function(x) gsub(' ', '_', x))
taxo_tot <- as.data.frame(t(parSapply(cl, as.data.frame(t(taxo_tot)), function(x){
  y <- as.character(x)
  for(i in 1:length(y)){
    if(grepl('^[[:lower:]]|affiliation', y[i])){
      y[i] <- 'unknown'
    }
  }
  for(i in 2:length(y)){
    if(y[i] == y[i-1] | (y[i] == 'unknown' & grepl('X$', y[i-1]) == F)){
      y[i] <- paste0(y[i-1], '_X')
    }
    if(y[i-1] == paste0(y[i], '_X') | (y[i] == 'unknown' & grepl('X$', y[i-1]))){
      y[i] <- paste0(y[i-1], 'X')
    }
  }
  return(y)
})))
dimnames(taxo_tot) <- list(row.names(ass_tot), tax_lev)

ass_tot <- data.frame(ass_tot, taxo=apply(taxo_tot, 1, function(x) paste(x, collapse=';')))

# taxo sort
ord_tax <- order(ass_tot$taxo)

mr_sort <- mr_tot[,ord_tax]
ass_sort <- ass_tot[ord_tax,]
taxo_sort <- taxo_tot[ord_tax,]

# remove chloroplast and mitochondria
ind_scrap <- grepl('Mitochondria|Chloroplast', ass_sort$taxonomy)

mr_sort <- mr_sort[,ind_scrap == F]
ass_sort <- ass_sort[ind_scrap == F,]
taxo_sort <- taxo_sort[ind_scrap == F,]

# communities ####
# get regex
regex_comm <- list(prokaryotes='*',
                    methanogens=c('Methanobacteriales',
                                  'Methanococcales',
                                  'Methanopyrales',
                                  'Methanofastidiosales',
                                  'Halobacterota',
                                  'Methanocellales',
                                  'Methanomicrobiales',
                                  'Methanonatronarchaeia',
                                  'Methanosaetaceae',
                                  'Methanimicrococcus',
                                  'Methanococcoides',
                                  'Methanohalobium',
                                  'Methanohalophilus',
                                  'Methanolobus',
                                  'Methanomethylovorans',
                                  'Methanosalsum',
                                  'Methanosarcina',
                                  'Methermicoccaceae',
                                  'Methanomethyliales',
                                  'Methanomassiliicoccales',
                                  'Thermogymnomonas'),
                                     
                    methanotrophs=c('Methylobacterium-Methylorubrum',
                                    'Methylocapsa',
                                    'Methylocella',
                                    'Methylocystis',
                                    'Methyloferula',
                                    'Methylosinus',
                                    'Methylovirgula',
                                    'Methyloceanibacter',
                                    'Methylococcaceae',
                                    'Methylohalobius',
                                    'Methylomarinovum',
                                    'Methylothermus',
                                    'Methylomonadaceae',
                                    'Methylacidiphilaceae',
                                    'ANME-1',
                                    'Archaeoglobus',
                                    'ANME-2a-2b',
                                    'ANME-2c',
                                    'ANME3',
                                    'Methanoperedenaceae',
                                    'Methylomirabilaceae'))


# make communities (packages, taxa)
n_comm <- names(regex_comm)

lst_comm <- NULL
for(i in seq_along(n_comm)) {
  
  print(n_comm[i])
  
  # get all OTU belonging to the community
  if(i == 2){
    rej <- c('ANME-2a-2b','Methanoperedenaceae')
  } else {
    rej <- NULL
  }

  mr <- mr_sort[,apply(as.matrix(taxo_sort), 1, function(x) {
    y <- sapply(strsplit(as.character(x), '_'), '[[', 1)
    any(y %in% regex_comm[[i]]) & all(!y %in% rej)
  })]

  if(i == 1){
    mr <- mr_sort
  }
  
  mr <- mr[rowSums(mr) != 0,colSums(mr) != 0]
  
  
  # abd rel ----
  
  thresh <- ifelse(i == 1, 10000, 30)
  
  mr_thresh <- mr[rowSums(mr) >= thresh,]
  mr_thresh <- mr_thresh[colSums(mr_thresh) != 0]
  
  nseq <- rowSums(mr_thresh)
  
  mr_abrel <- decostand(mr_thresh, method='total')
  
  ass_abrel <- droplevels(ass_sort[names(mr_abrel),])
  taxo_abrel <- droplevels(taxo_sort[names(mr_abrel),])
  
  env_abrel <- cbind.data.frame(env_tot2[row.names(mr_abrel),], nseq=nseq)

  system.time(d_comm_abrel <- vegdist(decostand(mr_abrel, 'hell'))) # ~45 sec
  # same result then vegdist(decostand(mr_thresh, 'hell))
  
  
  # clr ----
  
  m <- mr
  pa <- decostand(m, 'pa')
  rs <- 0
  cs <- 0
  
  while(min(rs) < thresh | min(cs) <= 1){
    
    ind_smp <- rowSums(m) >= thresh
    ind_otu <- colSums(pa) > 1
    
    m <- m[ind_smp,ind_otu]
    pa <- pa[ind_smp,ind_otu]
    
    rs <- rowSums(m)
    cs <- colSums(pa)
    
    print(c(min(rs), min(cs)))
    print(dim(m))
    
  }

  system.time(mr_gbm <- cmultReplPar(m, method='GBM', output='p-counts', nb_cpu = 15)) # ~75 sec
  mr_clr <- decostand(mr_gbm, 'clr')
  
  env_clr <- env_tot2[row.names(mr_clr),]
  
  ass_clr <- ass_sort[names(mr_clr),]
  taxo_clr <- taxo_sort[names(mr_clr),]

  d_comm_clr <- vegdist(mr_gbm, 'aitchison') # ~40 sec
  
  #---
  lst_comm[[n_comm[i]]] <- list(abrel=list(mr=mr_abrel, ass=ass_abrel, 
                                           taxo=taxo_abrel, env=env_abrel, dis=d_comm_abrel),
                                clr=list(mr=mr_clr, ass=ass_clr, 
                                         taxo=taxo_clr, env=env_clr, dis=d_comm_clr))
  
}

# table of region by habitat
e <- lst_comm$prokaryotes$abrel$env
t <- table(e$geo_loc_name, e$env_mat_OM)
t <- rbind(cbind(t,rowSums(t)), c(colSums(t), sum(t)))

xtable(t)


# table of region by habitat by guild for samples, OTU and sequences

tb_det <- lapply(n_comm, function(x) {
  mr <- lst_comm[[x]]$abrel$mr
  env <- lst_comm[[x]]$abrel$env
  
  sapply(grp_mat, function(y) sapply(grp_reg, function(z){
    
    ind <- grepl(z, env$geo_loc_name) & grepl(y, env$env_mat_OM)
    m <- mr[ind,]
    m <- m[,colSums(m) != 0]
    
    return(c(smp=nrow(m), otu=ncol(m), seq=sum(env$nseq[ind])))
    
  }))
  
})

tb_det <- abind(tb_det, along=1)
row.names(tb_det) <- rep(c('samples','OTUs','sequences'), length(n_comm)*length(grp_reg))

xtable(tb_det, digit=0)



# other little stuff ####

# find methanogen/troph
n_meth <- row.names(taxo_sort)[apply(taxo_sort, 1, function(x) as.logical(length(grep(paste(regex_comm[-1], collapse='|'), x))))]
n_mg   <- row.names(taxo_sort)[apply(taxo_sort, 1, function(x) as.logical(length(grep(paste(regex_comm['methanogen'], collapse='|'), x))))]
n_mt   <- row.names(taxo_sort)[apply(taxo_sort, 1, function(x) as.logical(length(grep(paste(regex_comm['methanotroph'], collapse='|'), x))))]
n_comm <- names(lst_comm)


#####
file <- paste0(dir_save, '00_lst_comm.Rdata')
save(lst_comm, mr_sort, ass_sort, env_tot, env_tot2, taxo_sort, lst_pal, regex_comm, regex_var, n_meth, n_mg, n_mt, n_comm,
     d_geo, lev_fact, grp_reg, grp_mat, file=file)

file <- paste0(dir_save, '00_lst_comm_short.Rdata')
save(lst_comm, file=file)



















