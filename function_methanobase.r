#%%%%
# function methanobase
#%%%%


# 00_prep ####

# graf sz
lar1=3.15
lar2=4.33
lar3=6.65


# parallelisation of cmultRep

cmultReplPar <- function (X, label = 0, method = c("GBM","CZM"), output = "p-counts",
                          frac = 0.65, threshold = 0.5, adjust = TRUE, t = NULL,
                          s = NULL, z.warning = 0.8, suppress.print = FALSE, 
                          delta = NULL, nb_cpu=1) 
{
  
  cl_fun <- makeSOCKcluster(nb_cpu)
  registerDoSNOW(cl_fun)
  
  X <- as.matrix(X)
  X <- as.data.frame(ifelse(X == label, NA, X))
  
  checkNumZerosCol <- apply(X, 2, function(x) sum(is.na(x)))
  if (any(checkNumZerosCol/nrow(X) == 1)) {
    stop(paste("Column(s) containing all zeros/unobserved values were found (check it out using zPatterns).", 
               sep = ""))
  } else {
    if (any(checkNumZerosCol/nrow(X) > z.warning)) {
      warning(paste("Column(s) containing more than ", 
                    z.warning * 100, "% zeros/unobserved values were found (check it out using zPatterns).\n                    (You can use the z.warning argument to modify the warning threshold).", 
                    sep = ""))
    }
  }
  checkNumZerosRow <- apply(X, 1, function(x) sum(is.na(x)))
  if (any(checkNumZerosRow/ncol(X) == 1)) {
    stop(paste("Row(s) containing all zeros/unobserved values were found (check it out using zPatterns).", 
               sep = ""))
  } else {
    if (any(checkNumZerosRow/ncol(X) > z.warning)) {
      warning(paste("Row(s) containing more than ", z.warning * 
                      100, "% zeros/unobserved values were found (check it out using zPatterns).\n(You can use the z.warning argument to modify the warning threshold).", 
                    sep = ""))
    }
  }
  N <- nrow(X)
  D <- ncol(X)
  n <- apply(X, 1, sum, na.rm = TRUE)
  
  if (method != "CZM") {
    
    alpha <- foreach (i = 1:N, .combine=rbind, .verbose=T) %dopar% {
      as.numeric(apply(X, 2, function(x) sum(x[-i], na.rm = T)))
    }
    dimnames(alpha) <- NULL
    
    t <- alpha/rowSums(alpha)
    if ((method == "GBM") && (any(t == 0))) {
      stop("GBM method: not enough information to compute t hyper-parameter,\nprobably there are columns with < 2 positive values.")
    }
  
    s <- 1/apply(t, 1, function(x) exp(mean(log(x))))
    
    repl <- t * (s/(n + s))
  
  } else {
    repl <- frac * matrix(1, ncol = D, nrow = N) * (threshold/n)
  }
  
  
  X2 <- t(apply(X, 1, function(x) x/sum(x, na.rm = T)))
  colmins <- apply(X2, 2, function(x) min(x, na.rm = T))
  adjusted <- 0
  
  for (i in 1:N) {
    if (any(is.na(X2[i, ]))) {
      z <- which(is.na(X2[i, ]))
      X2[i, z] <- repl[i, z]
      if (adjust == TRUE) {
        if (any(X2[i, z] > colmins[z])) {
          f <- which(X2[i, z] > colmins[z])
          X2[i, z][f] <- frac * colmins[z][f]
          adjusted <- adjusted + length(f)
        }
      }
      X2[i, -z] <- (1 - (sum(X2[i, z]))) * X2[i, -z]
    }
  }
  
  rn <- row.names(X)
  system.time(X <- as.data.frame(t(foreach(i=1:N, .combine=cbind, .verbose=T) %dopar% {
    x <- unlist(X[i,])
    if (any(is.na(x))) {
      zero <- which(is.na(x))
      pos <- setdiff(1:D, zero)[1]
      x[zero] <- as.integer(x[pos]) / X2[i, pos] * X2[i,zero]
    }
    return(x)
  })))
  row.names(X) <- rn
  
  res <- X
  if (suppress.print == FALSE) {
    if ((adjust == TRUE) & (adjusted > 0)) {
      cat(paste("No. adjusted imputations: ", adjusted, 
                "\n"))
    }
  }
  
  stopCluster(cl_fun)
  
  return(as.data.frame(res, stringsAsFactors = TRUE))
}


# function points on boxplot
pts_bxp <- function(x, col=rgb(0.5,0.5,0.5,0.5), pch=19, cex=0.5){
  points(gl(ncol(x), nrow(x)), unlist(x), col=col, pch=pch, cex=cex)
}

