#%%%%
# Methanobase MBC DDR
#%%%%

rm(list=ls())

setwd('~')

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(abind)
require(PMCMRplus)
require(multcompView)


# DIR LOAD ####

dir_proj <- 'Projets/Methanobase/'
dir_stat <- paste0(dir_proj, 'stat/')
dir_out  <- paste0(dir_stat, 'out/01_ddr/')
dir_save <- paste0(dir_stat, 'Rdata/') 

dir.create(dir_out, showWarnings=F)

source(paste0(dir_stat, 'function_methanobase.r'))

wdt1 <- 3.35
wdt2 <- 6.89

redo=T

load(paste0(dir_save, '00_lst_comm.Rdata')) # silva 132 swrm v1
lst_comm_d762a6d <- lst_comm
load(paste0(dir_proj, 'backup_286c3d8/00_lst_comm.Rdata')) # version from before the bug
lst_comm_286c3d8 <- lst_comm

# building of palettes for different regions and habitat
alp <- 1

pal_reg <- col2rgb(lst_pal$geo_loc_name)

pal_dbl_reg <- NULL
for(i in 1:3){
  pal_dbl_reg <- cbind(pal_dbl_reg, rowMeans(pal_reg[,-i]))
  colnames(pal_dbl_reg)[ncol(pal_dbl_reg)] <- paste(colnames(pal_reg)[-i], collapse='_')
}

pal_dbl_reg <- apply(cbind(pal_reg, pal_dbl_reg)/255, 2, function(x) rgb(x[1],x[2],x[3], alpha=alp))
n_dbl_reg <- names(pal_dbl_reg)

#---
pal_hab <- col2rgb(lst_pal$env_mat_OM)

combi <- combn(names(lst_pal$env_mat_OM), 2)

pal_dbl_hab <- NULL
for(i in 1:ncol(combi)){
  pal_dbl_hab <- cbind(pal_dbl_hab, rowMeans(pal_hab[,names(lst_pal$env_mat_OM) %in% combi[,i]]))
  colnames(pal_dbl_hab)[ncol(pal_dbl_hab)] <- paste(combi[,i], collapse='_')
}

pal_dbl_hab <- apply(cbind(pal_hab, pal_dbl_hab)/255, 2, function(x) rgb(x[1],x[2],x[3], alpha=alp))
n_dbl_hab <- names(pal_dbl_hab)
#

# DISTANCE MATRICES CALCULATION ####

file <- paste0(dir_save, '01_lst_ddr.Rdata')
file <- paste0(dir_save, '01_lst_ddr_286c3d8.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  
  lst_ddr <- NULL
  for(i in n_comm) { # loop the communities
    
    # for(j in names(lst_comm[[i]])) { # loop the community transformation
    j <- 'abrel'
    
      # d_comm <- lst_comm[[i]][[j]]$dis
      # env <- lst_comm[[i]][[j]]$env
      d_comm <- vegdist(decostand(lst_comm_286c3d8[[i]]$mr_abrel, 'hell'))
      env <- lst_comm_286c3d8[[i]]$env_abrel
      
      for(k in grp_mat) { # loop the materials
        
        for(l in grp_reg) { # loop the regions
          
          # selec smp
          ind_smp <- which(grepl(k, env$env_mat_OM) & grepl(l, env$geo_loc_name))
          
          # mr_sub <- mr[ind_smp,]
          # mr_sub <- mr_sub[,colSums(mr_sub) != 0]
          env_sub <- droplevels(env[ind_smp,])
          
          nr <- nrow(env_sub)
          rn <- row.names(env_sub)
          
          # check if the matrix is empty
          if(nr > 1){
          
            # regions and habitat factor for stratification and testing ----
            cmbn <- combn(1:nr, 2)
            f_reg <- apply(cmbn, 2, function(x) paste(sort(unique(env_sub$geo_loc_name[x])), collapse = '_'))
            f_hab <- apply(cmbn, 2, function(x) paste(unique(env_sub$env_mat_OM[x]), collapse = '_'))
            
            # distance comm and geo ----
            d_comm_sub <- as.dist(as.matrix(d_comm)[rn,rn])
            d_geo_sub <- as.dist(as.matrix(d_geo)[rn,rn])
            
            lst_ddr[[i]][[j]][[k]][[l]] <- list(d_comm=d_comm_sub, d_geo=d_geo_sub, f_reg=f_reg, f_hab=f_hab)
          }
        }
      }
    # }
  }
  
  save(lst_ddr, file=file)
  
}


# GRAF AND TABLE ####

wdt <- 7
hei <- 5

nb_boot <- 25

lst_l_coef <- NULL

cl <- makeSOCKcluster(15)
registerDoSNOW(cl)

for(i in n_comm){
  
  for(j in names(lst_ddr[[i]])){
    
    print(paste('##### model calculations', i, j))
  
    #############
    
    # get the data
    bh <- 1-c(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$bc_hell)
    bh <- 1-c(as.matrix(vegdist(decostand(lst_comm_d762a6d$prokaryotes$abrel$mr, method = 'hell'))))
    dg <- c(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$d_geo)
    
    rf <- factor(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$reg_fact)
    hf <- factor(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$hab_fact)
    
    rf <- factor(rf, level=names(pal_dbl_reg))
    
    # correct distance = 0 -> min
    min <- min(bh[bh != 0], na.rm=T)
    bh[bh <= min] <- min
    
    # log
    lbh <- log10(bh)
    ldg <- log10(dg+1)
    
    #############
    
    
    
    
    
    
    if(j == 'abrel'){
      dc <- 1-c(lst_ddr[[i]][[j]]$`[a-z]`$`Alaska|Patagonia|Siberia`$d_comm)
      
      # correct distance = 0 -> min
      min <- min(dc[dc != 0], na.rm=T)
      dc[dc <= min] <- min
      
    } else {
      dc <- c(lst_ddr[[i]][[j]]$`[a-z]`$`Alaska|Patagonia|Siberia`$d_comm)
    }
    
    dg <- c(lst_ddr[[i]][[j]]$`[a-z]`$`Alaska|Patagonia|Siberia`$d_geo)
    
    fr <- lst_ddr[[i]][[j]]$`[a-z]`$`Alaska|Patagonia|Siberia`$f_reg
    fr <- factor(fr, level=names(pal_dbl_reg))
    
    fh <- lst_ddr[[i]][[j]]$`[a-z]`$`Alaska|Patagonia|Siberia`$f_hab
    
    # log
    ldc <- log10(dc)
    ldg <- log10(dg+1)
    
    # strat
    strat <- factor(apply(cbind.data.frame(fr,fh), 1, function(x) paste(x, collapse='|')))
    lev <- levels(strat)
    
    for(k in grp_reg[-1]){
      cmb <- combn(grp_mat[-1], 2)
      for(l in 1:ncol(cmb)) {
        posib <- c(paste0(k, '|', cmb[1,l], '_', cmb[2,l]), paste0(k, '|', cmb[2,l], '_', cmb[1,l]))
        strat[strat == posib[1] | strat == posib[2]] <- posib[1]
      }
    }
    
    # get data.frame
    df <- droplevels(na.omit(data.frame(ldc=ldc, ldg=ldg, fr, fh, strat)))
    
    if(nrow(df) == 0) next
    
    
    # loop the habitat ----
    
    lst_coef <- foreach(k=0:nb_boot, .verbose=T) %dopar% {
      
      set.seed(k)
      
      # select samples for the bootstrap
      # note: samples selected rather then distance because if distance selected, all samples are involved
      # and there is not difference between the bootstrap from the phy-chim point of view
      if(k != 0){
        ind_boot <- row.names(df)[unlist((tapply(1:nrow(df), list(df$strat), function(x) sample(x, 0.8*length(x)))))]
      } else {
        ind_boot <- row.names(df)
      }
      
      df_boot <- df[ind_boot,]
      
      # calculate the models and save the coef ###
      lm <-  lm(ldc~ldg,    data=df_boot)
      lmh <- lm(ldc~ldg*fh, data=df_boot)
      l <- list(coef =coef(lm),
                coefh=coef(lmh))
      
      return(l)
    }
    
    #---
    lst_coef <- lapply(1:length(lst_coef[[1]]), function(x) sapply(lst_coef, '[[', x))
    names(lst_coef) <- c('full_model','model_vs_habitat')
    
    lst_coef[['df']] <- df
    
    lst_l_coef[[i]][[j]] <- lst_coef
    
  }
}


stopCluster(cl)




file <- 'Projets/Methanobase/backup_d762a6d/01_lst_l_coef_286c3d8.Rdata'
file <- 'Projets/Methanobase/backup_d762a6d/01_lst_l_coef_d762a6d.Rdata'
save(lst_l_coef, file=file)
load(file)
lst_l_coef_286c3d8 <- lst_l_coef
lst_l_coef_d762a6d <- lst_l_coef


# FIG DDR ####

transfo <- 'abrel'

#---
cairo_ps(paste0(dir_out, 'fig_ddr.eps'), width=lar3, height=lar3)

layout(matrix(c(1,1,2,3), nrow=2, byrow=T))
par(mar=c(5,5,3,0), oma=c(0,1,1,1))

#---
df <- lst_l_coef$prokaryotes[[transfo]]$df

xaxt <- 's'
ylab <- 'log Bray-Curtis similarity'

coef <- lst_l_coef$prokaryotes[[transfo]]$full_model

ssd <- round(c(coef[2,1], sd(coef[2,-1])), 4)

#---

plot(df$ldc~df$ldg, pch=19, cex=0.5, col='grey80', xlab='log geographic distance', ylab=ylab, xaxt=xaxt)
usr <- par('usr')

text(usr[1]+diff(usr[1:2])*0.1, usr[3]+diff(usr[3:4]*0.1), paste('slope =', ssd[1], '±', 
                                                                 format(ssd[2], scientific=F)), pos=4)

abline(coef[,1])

text(usr[1]-diff(usr[1:2])*0.05, usr[4]+diff(usr[3:4])*0.1, 'A', xpd=NA, font=2)


# slp vs guilds

slps_comm <- as.data.frame(sapply(lst_l_coef, function(x) x[[transfo]]$full_model[2,]))[-1,]
slps_comm <- as.data.frame(sapply(lst_l_coef, function(x) x[[transfo]]$full_model[2,]))[-1,]

pvs <- kwAllPairsNemenyiTest(slps_comm)$p.value
pvs <- as.dist(cbind(rbind(rep(NA,ncol(pvs)), pvs), rep(NA, nrow(pvs)+1)))
attributes(pvs)$Labels <- n_comm

mcl <- multcompLetters(pvs)$Letters

#---
boxplot(slps_comm, ylim=c(min(slps_comm), min(slps_comm)+diff(c(min(slps_comm),max(slps_comm)))*1.2), 
        ylab='DDR slopes', names=F)
usr <- par('usr')

pts_bxp(slps_comm)

text(1:ncol(slps_comm)+0.2, usr[3]-diff(usr[3:4]*0.05), names(slps_comm), adj=1, xpd=NA, srt=45)

text(1:length(slps_comm), usr[4]-diff(usr[3:4])*0.15, label=mcl, pos=3)

text(usr[1]-diff(usr[1:2])*0.1, usr[4]+diff(usr[3:4])*0.1, 'B', xpd=NA, font=2)


# slps vs habitat

coef_hab <- lst_l_coef$prokaryotes[[transfo]]$model_vs_habitat
coef_hab <- coef_hab[grep('ldg', row.names(coef_hab)),]
slps_hab_df <- cbind.data.frame(overall=lst_l_coef$prokaryotes[[transfo]]$model_vs_habitat['ldg',-1],
                                t(rbind(coef_hab[1,-1],t(apply(coef_hab[c(grep('_', row.names(coef_hab), invert=T))[-1],-1],
                                                               1, function(x) x+coef_hab[1,-1])))))

pvs <- kwAllPairsNemenyiTest(slps_hab_df)$p.value
pvs <- as.dist(cbind(rbind(rep(NA,ncol(pvs)), pvs), rep(NA, nrow(pvs)+1)))
attributes(pvs)$Labels <- c('overall', grp_mat[-1])

mcl <- multcompLetters(pvs)$Letters

col_ave <- floor(rowMeans(col2rgb(lst_pal$env_mat_OM)))/255
col_ave <- rgb(col_ave[1],col_ave[2],col_ave[3])

boxplot(slps_hab_df, ylim=c(min(slps_hab_df), min(slps_hab_df)+diff(c(min(slps_hab_df),max(slps_hab_df)))*1.2),
        names=F, col=c(col_ave, lst_pal$env_mat_OM), ylab='DDR slopes')
usr <- par('usr')

pts_bxp(slps_hab_df)

text(1:5+0.2, usr[3]-diff(usr[3:4]*0.1), c('overall',grp_mat[-1]), adj=1, srt=45, xpd=NA)

text(1:length(slps_hab_df), usr[4]-diff(usr[3:4])*0.15, label=mcl, pos=3)

text(usr[1]-diff(usr[1:2])*0.1, usr[4]+diff(usr[3:4])*0.1, 'C', xpd=NA, font=2)


dev.off()
#


# verification comment R1.2: Is slope steeper if smaller group? or Is slope change if random group of same size?

for(i in n_comm){
  
  # get the data
  bh <- 1-c(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$bc_hell)
  dg <- c(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$d_geo)
  rf <- factor(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$reg_fact)
  hf <- factor(lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$hab_fact)
  
  rf <- factor(rf, level=names(pal_dbl_reg))
  
  # correct distance = 0 -> min
  min <- min(bh[bh != 0], na.rm=T)
  bh[bh <= min] <- min
  
  # log
  lbh <- log10(bh)
  ldg <- log10(dg+1)
  
  # strat
  strat <- factor(apply(cbind.data.frame(rf,hf), 1, function(x) paste(x, collapse='|')))
  lev <- levels(strat)
  
  # decrease in diversity within the same guild
  
  
  if(i == 'prokaryote') next
  
  # full mod vs rndm OTU (same smps)
  # full mod vs rndm OTU & smp
  
  
}

#####























