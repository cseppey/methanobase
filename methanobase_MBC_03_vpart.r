#####
# Methanobase MBC rpart
#####

rm(list=ls())

setwd('~')

require(parallel)
require(venn)


# dir loads ####
dir_out <- 'Projets/Methanobase/stat/out/'
dir_save  <- paste0(dir_out, 'saves/') 
dir_vpart <- paste0(dir_out, '03_vpart/')
dir.create(dir_vpart, showWarnings=F)

#---
load(paste0(dir_save, '00_lst_comm2n.Rdata'))
load(paste0(dir_save, '01_lst_ddr2n.Rdata'))


lst_plot_03 <- NULL

redo=T

#%%%%%%%%%%%%%

# calculating the varpart ####

file <- paste0(dir_save, '03_lst_varpart.Rdata')
perc_cpu <- 1

if(file.exists(file) & redo == F){
  load(file)
} else {
  
  lst_vp <- NULL
  for(i in n_comm){
    print(paste('##### vpart', i))
    
    # mr <- lst_comm[[i]]$mr_abrel
    env_vpart <- lst_comm[[i]]$env_abrel[,c(paste0('PCNM', 1:5),
                                            'env_mat_OM','depth_water','dry_wgh','temp_deg_C','pH','OM',
                                            'diss_oxygen','diss_CH4_mg_L','redox_potential','conduc_mS_cm',
                                            paste0('bio_', c('01','05','08','12','13')))]
    
    env_sc <- as.data.frame(scale(env_vpart[,sapply(env_vpart, is.numeric)]))
    
    lst <- NULL
    for(j in grp_mat) {
      
      # env var
      ind_mo <- grep(j, env_vpart$env_mat_OM)
      env_mo <- env_vpart[ind_mo,]
      
      env_mo$diss_oxygen[row.names(env_mo) == 'SIL2AST'] <- NA ### ### ### ### ###
      
      # selection variable with less then 25% of missing values
      env_mo <- env_mo[,sapply(env_mo, function(x) length(which(is.na(x)))/length(x) <= 0.25)]
      
      env_mo <- na.omit(env_mo)
      tb_mat <- table(env_mo$env_mat_OM)
      
      # scaling the numeric variables and the mat factor
      env_mo <- scale(env_mo[,sapply(env_mo, is.numeric)])
      
      # # get the matrix used for the vp just to show the nb_smp and nb_otu in the graf
      # mr_mo <- mr[row.names(env_mo),]
      # mr_mo <- mr_mo[,colSums(mr_mo) != 0]
  
      # get the distance mat from the DDR and select the targetted samples
      bc_hell <- lst_ddr[[i]][[j]]$`Alaska|Patagonia|Siberia`$bc_hell
      bc_hell <- as.dist(bc_hell[row.names(env_mo),row.names(env_mo)])
      
      # grp_var_env
      lst_grp_var <- list(env_phy_chi=env_mo[,grep(regex_var['phy_chi'], colnames(env_mo))],
                          env_geo    =env_mo[,grep(regex_var['geo'],     colnames(env_mo))],
                          env_meteo  =env_mo[,grep(regex_var['meteo'],   colnames(env_mo))])
      
      # varpart
      vp <- varpart(bc_hell, lst_grp_var[[1]], lst_grp_var[[2]], lst_grp_var[[3]])
      
      # r2adj
      r2adj <- vp$part$indfract$Adj.R.square
      l_r2adj <- length(r2adj)
      
      # tests
      if(length(which(vp$part$indfract$Testable[1:3])) == 3 & unique(is.na(r2adj)) == F){ 
        # condition above related to the problem with methanogens in water: DF > nb_smp-1 for full model
        
        et_vp <- sign_vp <- matrix(nrow=2,ncol=length(lst_grp_var))
        hetero <- NULL
        h2 <- NULL
        for(k in 1:2){ # loop on strict and full variance
          
          sa <- seq_along(lst_grp_var)
          for(l in sa){ # loop on all variable type
            
            if(k == 1){
              formu <- as.formula(paste0('bc_hell~', paste(colnames(lst_grp_var[[l]]), collapse='+'), ' + Condition(', 
                                         paste(unlist(lapply(lst_grp_var[sa[-l]], colnames)), collapse='+'), ')'))
              
              print(paste('#####', i, j, 'strict', names(regex_var)[l]))
              
              sign_vp[k,l] <- s <- anova(dbrda(formu, data=as.data.frame(env_mo)), permutations=permu, 
                                         parallel=detectCores()*perc_cpu)$`Pr(>F)`[1]
              
              if(s < 0.001){et <- '***'
              } else if(s < 0.01){et <- '**'
              } else if(s < 0.05){et <- '*'
              } else if(s < 0.1){et <- '.'
              } else {et<-''}
              
              et_vp[k,l] <- et
              
              # calculation of heterogeneity
              sds <- sapply(env_sc[row.names(env_mo),colnames(lst_grp_var[[l]])], function(x) sd(x, na.rm=T))
              hetero[[names(lst_grp_var)[l]]] <- prod(sds)^(1/length(sds))
              h2[[names(lst_grp_var)[l]]] <- mean(sds)

            } else {
              
              print(paste('#####', i, j, 'total', names(regex_var)[l]))
              
              sign_vp[k,l] <- s <- anova(dbrda(bc_hell~lst_grp_var[[l]]), permutations=permu,
                                         parallel=detectCores()*perc_cpu)$`Pr(>F)`[1]
              
              if(s < 0.001){et <- '***'
              } else if(s < 0.01){et <- '**'
              } else if(s < 0.05){et <- '*'
              } else if(s < 0.1){et <- '.'
              } else {et<-''}
              
              et_vp[k,l] <- et
            }
            
          }
        }
        
        #---
        lst[[j]] <- list(r2adj=r2adj, 
                         # mr_mo=mr_mo,
                         env_mo=env_mo, tb_mat=tb_mat, lst_grp_var=lst_grp_var, 
                         sign_vp=sign_vp, et_vp=et_vp, hetero=hetero, h2=h2)
      }
    }
    
    lst_vp[[i]] <- lst
    lst_vp[[i]]$env_sc <- env_sc
  }
  
  save(lst_vp, file=file)
}


# graf ####
l_gm <- length(grp_mat)
l_co <- length(n_comm)

wdt=5*l_gm
hei=5*l_co

# venn ####
venn(3)
usr_v <- par('usr')

x11(wdt,hei)

layout(matrix(c(1,3:7, 1,8:12, 1,13:17, 0,rep(2,5)), nrow=4, byrow=T), c(0.2,rep(1,5)), c(rep(1,3), 0.2), respect=F)

# communities
par(mar=c(1,0,1,0))

plot.new()
usr <- par('usr')

y <- seq(usr[3],usr[4],length.out=l_co*2+1)
y <- y[1:length(y) %% 2 == 0]

text(0.9, y, rev(n_comm), srt=90, cex=2)
text(0.3,0.5, 'guilds', srt=90, cex=2.5)

# material
par(mar=c(0,1,0,1))

plot.new()
usr <- par('usr')

x <- seq(usr[3],usr[4],length.out=l_gm*2+1)
x <- x[1:length(x) %% 2 == 0]

text(x, 0.9, c('overall',grp_mat[-1]), cex=2)
text(0.5,0.3, 'materials', cex=2.5)

# normalisztion of r2adj in log for the color shades
ul_vp <- unlist(lapply(lst_vp, function(x) lapply(x, function(y) rev(rev(y$r2adj)[-1]))))
ul_vp <- ifelse(ul_vp <= 0, 0, ul_vp) # to check it 0.0001 should not be0
ul_vp <- ul_vp-min(ul_vp)
ul_vp <- ul_vp*(1/max(ul_vp))

# venn
for(i in n_comm){
  
  for(j in grp_mat){
    print(paste('##### venn', i, j))
    
    r2adj       <- lst_vp[[i]][[j]]$r2adj
    n <- sapply(names(ul_vp), function(x) substr(as.character(x), 0, nchar(x)-1))
    r2adj_col   <- ul_vp[n == paste0(i, '.', j)]
    et_sign     <- lst_vp[[i]][[j]]$et_vp
    if(is.null(r2adj)){plot.new(); next}
    # mr_mo       <- lst_vp[[i]][[j]]$mr_mo
    tb_mat      <- lst_vp[[i]][[j]]$tb_mat
    lst_grp_var <- lst_vp[[i]][[j]]$lst_grp_var
    
    l_r2adj <- length(r2adj)
    
    #---
    plot.new()
    plot.window(usr_v[1:2],usr_v[3:4])
    
    # unexplained variance
    rect(usr_v[1], usr_v[3], usr_v[2], usr_v[4], 
         col='white')

    text(usr_v[1] + diff(usr_v[1:2])*0.5, usr_v[3] + diff(usr_v[3:4])*0.1,
         paste('residuals =', round(r2adj[l_r2adj], 2)))
    
    # variables used
    legend(usr_v[1] + diff(usr_v[1:2])*0.1, usr_v[3] + diff(usr_v[3:4])*0.99,
           unlist(lapply(lst_grp_var, colnames)), xjust=0.5, yjust=1,
           border=F, bty='n')
    
    # group of variables
    text(usr_v[1]+diff(usr_v[1:2])*c(0.85,0.5,0.15), usr_v[3]+diff(usr_v[3:4])*c(0.2,0.85,0.2),
         c('physico\nchemistry','geography','climate'))
         # paste0(names(lst_grp_var), ' (', paste0('X', 1:3, ' '), sapply(lst_grp_var, ncol), ')'))

    # color the zones
    for(k in 1:(l_r2adj-1)){
      k2 <- c(1,2,4,3,6,5,7)[k]
      gz <- getZones(k2,3)
      r <- round(r2adj[k], 2)
      
      r_col <- round(r2adj_col[k], 2)
      r_col <- ifelse(r_col <= 0, 0, r_col)
      col <- switch(k,
                    '1'  = 'cyan',
                    '2'  = 'magenta',
                    '3'  = 'yellow',
                    '4'  = 'blue',
                    '5'  = 'red',
                    '6'  = 'green',
                    '7'  = 'black',
                    'NA' = 'white')
      col <- c(col2rgb(col))/255
      col <- rgb(col[1], col[2], col[3], r_col)
      
      # polygon(gz[[1]], col='white')
      polygon(gz[[1]], col=col)
      
      if(k <= 3){
        r <- paste(et_sign[1,k], r, sep='\n')
      }
      text(getCentroid(gz)[[1]][1], getCentroid(gz)[[1]][2], r,
           col=ifelse(col2rgb(col, alpha=T)[4,1] > 255/2 & k == 7, 'white', 1))
    }
    
  }
}

lst_plot_03[['full_venn']] <- recordPlot()

dev.off()


# graf barplot ####
bars <- sapply(lst_vp, function(x) sapply(x[names(x) != 'env_sc'], function(y) c(y$r2adj[1:3], 1-y$r2adj[8]-sum(y$r2adj[1:3]))))
bars <- cbind(bars[[1]],bars[[2]],bars[[3]])
dimnames(bars) <- list(c(names(lst_pal$variables)[c(2,1,3)], 'shared'), sub('[a-z]', 'overall', colnames(bars), fixed=T))

et_vp <- sapply(lst_vp, function(x) sapply(x[names(x) != 'env_sc'], function(y) y$et_vp)[1:3,])
et_vp <- cbind(et_vp[[1]],et_vp[[2]],et_vp[[3]])[c(1,3,2),]
dimnames(et_vp) <- list(row.names(bars)[c(1,3,2)], colnames(bars))

# stacked
b <- barplot(bars, plot=F)

cs <- apply(bars, 2, function(x) {
  y <- rev(x)
  if(y[1] < 0){
    y <- c(y[1],0,cumsum(y[2:4]))
  } else {
    y <- c(0,cumsum(y))
  }
  return(y)
})

cs_mid <- apply(bars, 2, function(x) {
  x <- ifelse(x < 0, 0, x)
  y <- NULL
  for(i in 1:3){
    y <- c(y, 0.5*x[i] + sum(x[((i+1):length(x))]))
  }
  return(y)
})
  
xshft <- diff(b[1:2])*0.3


#----
x11(lar3, lar3/1.5)

par(mar=c(5,5,1,1), mgp=c(2,1,0))

plot.new()
plot.window(xlim=range(b), ylim=range(cs))
usr <- par('usr')

for(i in 2:nrow(cs)){
  rect(b-xshft, cs[i-1,], b+xshft, cs[i,], col=c('grey', lst_pal$variables[c(3,1,2)])[i-1], border=F)
}

segments(usr[1],0, usr[2],0)

xs <- b[c(5,8)]+diff(b[1:2])/2
segments(xs,usr[3]-diff(usr[3:4]*0.3), xs, usr[4], xpd=T)

mtext(colnames(cs), 1, 0.5, at=b, cex=0.75, las=2)
mtext(n_comm, 1, 4, at=b[c(3,7,11)])

axis(2)
mtext('explained variation %', 2, 3)

legend('topleft', legend=c('physicochemistry','geography','climate','multiple groups\nvariation'), 
       col=c('cyan','magenta','yellow','grey'), pch=19, bty='n', cex=0.5, inset=0.05)

lst_plot_03[['barplot_stack']] <- recordPlot()

dev.off()


# beside ----
x11(lar3, lar3/2)

par(mar=c(5,4,1,1), mgp=c(2,1,0))

b <- barplot(bars, col=c(lst_pal$variables[c(2,1,3)], 'grey'), border=F, cex.names=0.5, cex.axis=0.5, ylab='variation %', beside=T)
usr <- par('usr')

mtext(n_comm, 1, 3, at=colMeans(b[,c(3,7,11)]))
abline(h=0)

xs <- b[1,c(6,9)]-1
segments(xs,usr[3]-diff(usr[3:4]*0.3), xs, usr[4], xpd=T)

lst_plot_03[['barplot_side']] <- recordPlot()

dev.off()



#####


save(lst_plot_03, file=paste0(dir_save, '03_lst_plot.Rdata'))

















