#####
# Methanobase MBC rpart
#####

rm(list=ls(all.names=T))

setwd('~')

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(parallel)
require(rpart.utils) # rpart.subrules.table, rpart.rules.table
require(rpart.plot) # prp
require(mvpart)
require(labdsv)
require(plotrix)

source('~/bin/src/my_prog/R/pie_taxo.r')
source('~/bin/src/my_prog/R/pie_taxo_single.r')
source('~/bin/src/my_prog/R/legend_pie_taxo.r')

# prep cluster
nb_cpu <- detectCores()

if(exists('cl')){
  stopCluster(cl)
}

cl <- makeSOCKcluster(nb_cpu)

clusterEvalQ(cl, library(labdsv))

registerDoSNOW(cl)

# dir loads ####
dir_out <- 'Projets/Methanobase/stat/out/'
dir_save  <- paste0(dir_out, 'saves/') 
dir_rpart <- paste0(dir_out, '02_rpart/')
dir.create(dir_rpart, showWarnings=F)

#---
load(paste0(dir_save, '00_lst_comm.Rdata'))
load(paste0(dir_save, '01_lst_ddr.Rdata'))

lst_plot_02 <- NULL

redo <- T

# tax_lev
lst_tax_lev <- list(prokaryotes=1:5, methanotrophs=3:6, methanogens=2:6)

# ray max
r_max <- 0.03

# tree function
make_tree <- function(rp, reg_var=regex_var, main=NULL, col=lst_pal$variables, return=F, cex=2, ...){
  tree <- prp(rp, type=5, under=T, branch=1, nn=F, main=main, xcompact=F, ycompact=F, cex=cex, ...)
  
  coord_bx_tree <- data.frame(tree$labs, as.data.frame(tree$boxes), tree$split.box$x1)
  coord_bx_tree <- coord_bx_tree[is.na(coord_bx_tree$tree.split.box.x1) == F,]
  
  for(j in seq_along(reg_var)){
    ind_smp <- grepl(reg_var[j], coord_bx_tree$tree.labs) | sapply(coord_bx_tree$tree.labs, function(x) grepl(x, reg_var[j]))  
    
    if(length(which(ind_smp))){
      apply(coord_bx_tree[ind_smp,2:5], 1, function(x) rect(x[1], x[2], x[3], x[4], col=col[j]))
      text(apply(coord_bx_tree[ind_smp,c(2,4)], 1, mean), apply(coord_bx_tree[ind_smp,c(3,5)], 1, mean), 
           coord_bx_tree$tree.labs[ind_smp], cex=cex)
    }
  }
  
  if(return){
    return(tree)
  }
}


# rpart and iv ####
lst_var <- NULL
for(i in n_comm){
  
  bc_hell <- lst_ddr[[i]]$`[a-z]`$`Alaska|Patagonia|Siberia`$bc_hell2 #### main difference with before, the data are not squared anymore (with gdist)
  
  mr <- lst_comm[[i]]$mr_abrel

  env <- lst_comm[[i]]$env_abrel
  env_rpart <- env[,c(paste0('PCNM', 1:5),
                      'depth_water','dry_wgh','temp_deg_C','pH','OM',
                      'diss_oxygen','diss_CH4_mg_L','redox_potential','conduc_mS_cm',
                      paste0('bio_', c('01','05','08','12','13')))]
  
  names(env_rpart)[c(6,8,11:14)] <- c('depth','temp','diss_O2','dis_CH4','redox','conduc')
  
  lst_var[[i]]$env_rpart <- env_rpart
  
  taxo <- lst_comm[[i]]$taxo_abrel

  tax_lev <- lst_tax_lev[[i]]
  
  # rpart ####
  print(paste('##### rpart', i))
  
  rpart_pr <- rpart(bc_hell~., data=env_rpart, method='dist', maxdepth=3)
  
  # definition of nodes rules
  rules_table <- rpart.rules.table(rpart_pr)
  rules_table <- tapply(rules_table$Subrule, list(droplevels(rules_table$Rule)), function(x) as.character(x))
  
  sub_rules <- rpart.subrules.table(rpart_pr)
  sub_rules <- lapply(rules_table, function(x) sub_rules[grep(paste0(x, '$', collapse='|'), sub_rules$Subrule),])
  
  node_rules <- NULL
  for(j in seq_along(sub_rules)){
    conds <- NULL
    
    # get the rules and sorting it according to the first in the tree
    lev_sr <- levels(droplevels(sub_rules[[j]]$Subrule))
    lev_sr <- lev_sr[order(as.numeric(substr(unique(as.character(sub_rules[[j]]$Subrule)), 2, 
                                             nchar(as.character(sub_rules[[j]]$Subrule)))))]
    
    if(length(lev_sr)){ # if the rule is not the tree root
      for(k in lev_sr){
        
        sub_rule <- sub_rules[[j]][sub_rules[[j]]$Subrule == k,]
        
        val <- unique(sub_rule[,3:5][is.na(sub_rule[,3:5]) == F])
        
        cond <- paste(c('<','>','=')[is.na(sub_rule[1,c(4,5,3)]) == F], collapse='')
        cond <- paste0('env_rpart$', sub_rule$Variable, ' ', cond, ' ', val)
        
        conds <- c(conds, cond)
      }
    
    } else { # if tree root
      conds <- 'rep(T, nrow(env_rpart))'
    }
    
    node_rules <- c(node_rules, paste(conds, collapse=' & '))
  }
  names(node_rules) <- names(sub_rules)
  
  lst_var[[i]]$node_rules <- node_rules
  lst_var[[i]]$rpart_pr <- rpart_pr
  
  # file smps
  
  file <- paste0(dir_rpart, 'smp_node_', i, '.csv')
  if(file.exists(file)){file.remove(file)}
  
  for(k in seq_along(node_rules)[-1]){
    write.table(matrix(c(paste0('(', names(node_rules)[k], ') ', node_rules[k]),
                         na.omit(row.names(env_rpart)[eval(parse(text=node_rules[k]))])), nrow=1), file, T, F, '\t', col.names=F)
  }
  
  # nb of sample per nodes
  nb_smp_node_rules <- sapply(node_rules, function(x) length(which(eval(parse(text=x)))))
  
  rays <- sqrt(r_max^2*pi / max(nb_smp_node_rules) * nb_smp_node_rules / pi)
  #
  
  # iv vs nodes ####
  print(paste('##### iv vs nodes', i))
  
  p_tresh <- 0.001
  
  # selection of the OTU representing more then a 10000 th of the community
  ind_red <- colMeans(mr) >= 0.0001
  mr_red <- mr[,ind_red]
  taxo_red <- taxo[ind_red,]

  #---
  file <- paste0(dir_save, '02_lst_selec_', i, '.Rdata')
  
  if(file.exists(file) & redo == F){
    load(file)
  } else {
    
    lst_selec <- foreach(j=seq_along(node_rules), .export='env_rpart', .verbose=T) %dopar% {
      print(node_rules[j])
      print(round(j/length(sub_rules), digit=2))
  
      # selection of the samples
      grp <- eval(parse(text=node_rules[j]))
      grp_nna <- ifelse(is.na(grp), F, grp)
  
      # selection of the samples
      mr_nna <- mr_red
      mr_nna <- mr_nna[,colSums(mr_nna) != 0]
  
      # indval
      set.seed(0)
      iv <- indval(mr_nna, grp_nna, niter=permu)
  
      # select iv
      ind_iv <- iv$maxcls == 2 & iv$pval <= p_tresh
      if(j == 1){
        ind_iv <- iv$maxcls == 1
      }
      mr_iv <- as.data.frame(mr_nna[grp_nna,ind_iv])
      if(ncol(mr_iv) == 1){
        dimnames(mr_iv) <- list(row.names(mr_nna)[grp_nna], names(mr_nna)[ind_iv])
      }
      taxo_iv <- taxo[names(mr_iv),]
  
      iv_nodes <- list(mr_iv=mr_iv, taxo_iv=taxo_iv) # for the IVs stats after the plotting
  
      #---
      selec_otu <- row.names(iv$relfrq)[ind_iv]
      selec_smp <- ifelse(is.na(grp), F, grp)
  
      return(list(selec_smp=selec_smp, selec_otu=selec_otu, iv_nodes=iv_nodes))
    }
    names(lst_selec) <- names(node_rules)
    
    save(lst_selec, file=file)
  }
  
  selec_smp <- lapply(lst_selec, '[[', 1)
  selec_otu <- lapply(lst_selec, '[[', 2)
  iv_nodes  <- lapply(lst_selec, '[[', 3)
  
  
  
  # graf ####
  print(paste('##### plot', i))
  
  #---
  for(j in c('Bacteria','Archaea')){
    
    # get out of impossible situation
    if((i == 'methanogens' & j == 'Bacteria') | (i == 'methanotrophs' & j == 'Archaea')) next
      
    # select_reign
    ind_r <- taxo_red$reign == j
    mr_red_r <- mr_red[,ind_r]
    taxo_red_r <- droplevels(taxo_red[ind_r,])
    selec_otu_r <- sapply(selec_otu, function(x) x[x %in% names(mr_red_r)])
    
    # calculate the percentage of non IV in the reduced community
    # for(k in seq_along(selec_smp)[-1]){
    for(k in seq_along(selec_smp)){
      m_iv <- mr_red_r[selec_smp[[k]],selec_otu_r[[k]]]
      if(is.numeric(m_iv)){
        m_iv <- as.data.frame(m_iv)
        dimnames(m_iv) <- list(row.names(mr_red)[selec_smp[[k]]],selec_otu_r[[k]])
      }
      if(ncol(m_iv) == 0){next}
      
      m_tot <- mr_red[selec_smp[[k]],ind_r]
      
      non_iv <- rep(0, nrow(mr_red_r))
      non_iv[selec_smp[[k]]] <- rowSums(m_tot) - rowSums(m_iv)
      
      # add the perc iv to the mr_red_r and selec_otu_r
      non_iv_n <- paste0('non_iv_', k)
      mr_red_r <- eval(parse(text=paste0('cbind.data.frame(mr_red_r, ', non_iv_n, '=non_iv)')))
      selec_otu_r[[k]] <- c(selec_otu_r[[k]], non_iv_n)
      taxo_red_r <- eval(parse(text=paste0('as.data.frame(rbind(as.matrix(taxo_red_r),', non_iv_n, 
                                           '=c(j, rep(\'non-bioindicator\', ncol(taxo_red_r)-1))))')))
    }
    
    # pies
    if(j == 'Bacteria'){
      pal_ini <- colorRampPalette(c('red','green','blue'))
    } else {
      pal_ini <- colorRampPalette(c('cyan','yellow','magenta'))
    }
    
    thresh <- 0.05
    if(i == 'prokaryotes' & j == 'Archaea' & thresh < 0.05){
      add_pal <- 'grey80'
    } else {
      add_pal <- c('grey20', 'grey80')
    }
    
    pie <- pie_taxo(mr_red_r, taxo_red_r, tax_lev, selec_smp, selec_otu_r, show=F, 
                    last_tax_text=F, thresh=thresh, root=j, pal_ini=pal_ini, other=F, 
                    rare='bioindicator taxa', add_pal=add_pal, undet='non-bioindicator')
    
    lst_var[[i]][[j]] <- list(mr_red_r=mr_red_r, taxo_red_r=taxo_red_r, selec_otu_r=selec_otu_r)
    
    
    #---
    x11(lar3,lar3)
    
    layout(matrix(1:3), height=c(0.25,1,0.75))
    
    par(xpd=NA, mar=c(0,3,0,2))

    plot.new()
    
    # tree ----
    tree <- make_tree(rpart_pr, return=T, split.yshift=10, cex=0.75) #######
    usr <- par('usr')
    
    # bar and pie on tree
    fact <- c('geo_loc_name','env_mat_OM')
    for(k in seq_along(node_rules)){

      x <- tree$branch.x[1,k]
      y <- tree$branch.y[2,k]
      if(k == 1){
        y <- tree$branch.y[1,1]+ diff(tree$branch.y[1,2:1])
      }

      # pie ----
      nb_tax_lev <- max(which(sapply(pie$agg, is.factor)))

      pie_taxo_single(pie, k+nb_tax_lev, x, y, ray=r_max, last_tax_text=F, info_perc=F,
                      info_tax=T, cex=0.6, rshift=0.01)                                   ############

      # geo loc + env mat ----
      smp_node <- eval(parse(text=node_rules[k]))
      
      for(l in fact){

        # bar
        cs <- cumsum(table(env[[l]][smp_node]))

        y_rng <- y-diff(usr[3:4])*0.12 + c(-0.5,0.5)*r_max*2
        ys <- c(y_rng[1], cs/(max(cs))*diff(y_rng) + y_rng[1])

        if(l == 'geo_loc_name'){
          xs <- c(x-rays[k]*2, x)
        } else if(l == 'env_mat_OM') {
          xs <- c(x, x+rays[k]*2)
        } else {
          xs <- c(x+rays[k]*2, x+rays[k]*3)
        }

        for(m in seq_along(cs)){
          rect(xs[1], ys[m], xs[2], ys[m+1], border=NA, col=lst_pal[[l]][m])
        }

      }

      text(x, mean(range(ys)), length(which(smp_node)))
    }

    # legend factor and nb_smp ---
    yini <- usr[4]+diff(usr[3:4])*0.1
    xshift <- diff(usr[1:2])*0.05

    # fact
    ys <- c(yini, yini-diff(usr[3:4])*0.1)
    x <- usr[2]-xshift*2

    for(k in seq_along(fact)){
      legend(x, ys[k], names(lst_pal[[k]]),
             col=lst_pal[[fact[k]]], pch=19, bty='n', xjust=0, yjust=1, cex=0.75) #########
    }

    # nb smp
    ys <- seq(yini, yini-diff(usr[3:4]*0.4), length=5)
    x <- usr[1]+xshift

    rng <- range(rays)*2
    sz <- seq(rng[2],rng[1],length=5)

    text(x, ys, paste('n =', signif(seq(max(nb_smp_node_rules), min(nb_smp_node_rules), length.out=5), 2)),
         cex=0.75, pos=3, offset=0.2)                                                                         ##############

    for(k in seq_along(ys)){
      rect(x-sz[k], ys[k]-r_max*2, x+sz[k], ys[k], col=1, border=NA)
    }

    # legend taxo ----
    par(mar=rep(0,4))

    plot.new()
    legend_pie_taxo(pie, 0.5, 0.5, cex=0.6, last_tax_text=F) #########

    lst_plot_02[[paste(i,j, sep='_')]] <- recordPlot()
    
    dev.off()
    
  }
  
  # qq stats ####
  print(paste('##### qq stats', i))
  
  for(j in c('Bacteria','Archaea')) {
    
    # get out of impossible situation
    if((i == 'methanogens' & j == 'Bacteria') | (i == 'methanotrophs' & j == 'Archaea')) next

    mr_red_r <- lst_var[[i]][[j]]$mr_red_r
    taxo_red_r <- lst_var[[i]][[j]]$taxo_red_r
    selec_otu_r <- lst_var[[i]][[j]]$selec_otu_r
    
    # write infos
    file <- paste0(dir_rpart, 'iv_prop_', i, '_', j, '.csv')
    if(file.exists(file)){file.remove(file)}

    write.table(t(c('relative abundance within node for the reign','relative abundance within node bioindicator for the reign',
                    'reign','phylum','class','order','family',
                    'genus','species','seq')),file, T, F, '\t', col.names=F)
    
    for(k in seq_along(iv_nodes)){
    
      mr <- mr_red_r[selec_smp[[k]],selec_otu_r[[k]]]  
      
      taxo_iv <- taxo_red_r[names(mr)[-ncol(mr)],]
      
      n_iv <- row.names(taxo_iv)
      
      if(ncol(mr)){
        
        red_abrel <- colSums(mr)/sum(mr)
        red_abrel <- red_abrel[-length(red_abrel)]
        iv_abrel <- colSums(as.data.frame(mr[,-ncol(mr)]))/sum(mr[,-ncol(mr)])
        df <- cbind.data.frame(red_abrel, iv_abrel, taxo_iv, seq=lst_comm[[i]]$ass_abrel[n_iv,"seq"])
        
        row.names(df) <- n_iv
        
        write.table(paste0('(', names(node_rules)[[k]], ') ', node_rules[[k]]), file, T, T, '\t', row.names=F, col.names=F)
        
        write.table(df, file, T, F, '\t', col.names=F)
        write.table('\n', file, T, F, '\t', row.names=F, col.names=F)
      }
    }
    
  }
}


#---
save(lst_var, file=paste0(dir_save, '02_lst_var.Rdata'))


# retreive the samples per node ####
smps_per_node <- sapply(lst_var, function(x){
  
  rules <- x$node_rules
  env_rpart <- x$env_rpart
  
  smps <- sapply(rules, function(y){
    s <- row.names(env_rpart)[eval(parse(text=y))]
    return(s[!is.na(s)])
  })
  
  return(smps)
  
})

file <- paste0(dir_rpart, 'smps_per_node.csv')
if(file.exists(file)){
  file.remove(file)
}

for(i in names(smps_per_node)){
  write.table(paste('', i, '%%%%%', sep='\n'), file, T, F, row.names = F, col.names = F)
  for(j in names(smps_per_node[[i]])){
    write.table(paste('', paste('node', j), '###', sep='\n'), file, T, F, row.names = F, col.names = F)
    write.table(smps_per_node[[i]][[j]], file, T, F, row.names = F, col.names = F)
  }
}

#####

save(lst_plot_02, file=paste0(dir_save, '02_lst_plot.Rdata'))










